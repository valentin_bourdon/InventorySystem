using System;
using UnityEngine;

namespace InventorySystem
{
	[Serializable]
	public class ItemStack
	{
		[SerializeField]
		private Item _item;

		public Item Item => _item;

		public ItemStack()
		{
			_item = null;
		}
		public ItemStack(Item item)
		{
			_item = item;
		}
	}
}



