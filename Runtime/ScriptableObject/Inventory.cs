using System.Collections.Generic;
using UnityEngine;

namespace InventorySystem
{
	[CreateAssetMenu(fileName = "Inventory", menuName = "Inventory/Inventory")]
	public class Inventory : ScriptableObject
	{
		[Tooltip("The collection of items.")]
		[SerializeField]
		private List<ItemStack> _items = new List<ItemStack>();

		public List<ItemStack> Items => _items;

		private void OnEnable()
		{
			_items = new List<ItemStack>();
		}

		public void Add(Item item)
		{
			_items.Add(new ItemStack(item));
		}

		public void Remove(Item item)
		{
			for (int i = 0; i < _items.Count; i++)
			{
				ItemStack currentItemStack = _items[i];

				if (currentItemStack.Item == item)
				{
					_items.Remove(currentItemStack);
					return;
				}
			}
		}

		public bool Contains(Item item)
		{
			for (int i = 0; i < _items.Count; i++)
			{
				if (item == _items[i].Item)
				{
					return true;
				}
			}

			return false;
		}
	}
}

