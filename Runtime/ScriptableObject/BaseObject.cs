using UnityEngine;
using UnityEngine.Localization;

namespace InventorySystem
{
	public class BaseObject : SerializableScriptableObject
	{
		[Tooltip("The name of the object")]
		[SerializeField] private LocalizedString _name = default;

		[Tooltip("A description of the object")]
		[SerializeField]
		private LocalizedString _description = default;

		public LocalizedString Name => _name;
		public LocalizedString Description => _description;
	}
}