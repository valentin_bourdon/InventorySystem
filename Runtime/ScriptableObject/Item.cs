using UnityEngine;

namespace InventorySystem
{
	[CreateAssetMenu(fileName = "Item", menuName = "Inventory/Item", order = 51)]
	public class Item : BaseObject
	{
		[Tooltip("A preview image for the item")]
		[SerializeField]
		private Sprite _previewImage = default;
		public Sprite PreviewImage => _previewImage;

		[Tooltip("The cost of the item to buy it on the store")]
		[SerializeField]
		private int _cost = default;
		public int Cost => _cost;
	}
}


