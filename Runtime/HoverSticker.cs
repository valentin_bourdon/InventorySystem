using UnityEngine;
using TMPro;
using UnityEngine.UI;

/// <summary>
/// Show description of the item when hover it.
/// </summary>
/// 
namespace InventorySystem
{
    public class HoverSticker : MonoBehaviour
    {
        [Header("Item")]
        private Item _item;
        public Item Item
        {
            get => _item;
            set => _item = value;
        }

        [Header("Tooltip")]
        [SerializeField] private CanvasGroup _tooltip;
        [SerializeField] private TextMeshProUGUI _title;
        [SerializeField] private TextMeshProUGUI _description;

        public CanvasGroup Tooltip
        {
            get => _tooltip;
            set => _tooltip = value;
        }

        public TextMeshProUGUI Title
        {
            get => _title;
            set => _title = value;
        }

        public TextMeshProUGUI Description
        {
            get => _description;
            set => _description = value;
        }

        public void OnHover()
        {
            if (_item == null)
                return;

            if (!_item.Name.IsEmpty)
                _title.text = _item.Name.GetLocalizedString();
            if (!_item.Description.IsEmpty)
                _description.text = _item.Description.GetLocalizedString();

            _tooltip.alpha = 1;
            _tooltip.transform.position = RectTransformUtility.WorldToScreenPoint(Camera.main, transform.position);
        }

        public void OnExit()
        {
            _tooltip.alpha = 0;
        }
    }
}

