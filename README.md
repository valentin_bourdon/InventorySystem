# Inventory System Package
A simple example to use an inventory system.

## Requirements
* Unity 2020.3 or later.

## Dependencies
* ```"com.unity.textmeshpro": "3.0.6"```
* ```"com.unity.localization": "1.0.5"```

## Installing the Package
* Add the name of the package (found in **package.json**) followed by the repository **URL** to your Unity project's manifest.json or using the package manager in Unity.
* On **ProjectSettings>Localization**
  * Create a new **Localization Settings.asset**
  * Add your languages in **Available Locales**

```json
{
  "dependencies": {
    "com.bourdonvalentin.inventory-system": "https://gitlab.com/valentin_bourdon/InventorySystem.git",
}
```

## License

All the changes made are released under the MIT License, see [LICENSE](./LICENSE).
